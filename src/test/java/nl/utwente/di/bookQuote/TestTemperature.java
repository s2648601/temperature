package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** Tests the conversion */
public class TestTemperature {

    @Test
    public void testTemp() throws Exception {
        Converter converter = new Converter();
        double price = converter.toFahrenheit("20");
        Assertions.assertEquals(68, price);
    }
}