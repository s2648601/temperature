package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Converter {

    public double toFahrenheit(String celsius){
        return (Double.parseDouble(celsius)*1.8) + 32;
    }
}